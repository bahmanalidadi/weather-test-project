var axios = require('axios');
const cron = require('node-cron');
const redis = require('redis')
var express = require('express');

const client = redis.createClient(6379);

var app = express();

/**
 * Get Tehran Weather api
 */
app.get('/get-tehran-weather', function (req, res) {
    return client.get('tehran-weather', (err, data) => {
        // If tehran-weather key exists in Redis store
        if (data) {
            return res.status(200).send(JSON.parse(data));
        } else {
            return res.status(404).send('Tehran weather data not found!');
        }
    });
});

cron.schedule('*/1 * * * *', function () {
    console.log('cron started at');
    // use axios for get forcast of tehran
    axios.get("http://api.openweathermap.org/data/2.5/weather?q=Tehran&appid=f3f30b1dde9650d97b3f85bccaa47fd4")
        .then(weather_data => {
            client.setex('tehran-weather', 1800, JSON.stringify(weather_data.data)); // store weather data for 30 minutes
        });
});

app.listen(4000, function () {
  console.log('Weather app listening on port 4000!');
})
